import React from 'react';
import { ChatEngine } from 'react-chat-engine';

export default function ChatAdmin() {
  return (
    <ChatEngine
      projectID={process.env.REACT_APP_CE_PROJECT_ID}
      userName={'AdaoBJr'}
      userSecret={'1234'}
      height="calc(100vh - 12px"
    />
  );
}
