import { Switch, Route } from 'react-router-dom';

import Chat from './Chat';
import ChatAdmin from './ChatAdmin';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Chat} />
      <Route exact path="/chat" component={ChatAdmin} />
    </Switch>
  );
}

export default App;
