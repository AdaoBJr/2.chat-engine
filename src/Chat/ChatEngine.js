import React, { useState, useEffect } from 'react';
import { styles } from './styles';

import { ChatEngineWrapper, Socket, ChatFeed } from 'react-chat-engine';

export default function ChatEngine({ visible, user, chat }) {
  const [showChat, setShowChat] = useState(false);

  // CICLO DE VIDA -----------------------------------------------------------------------------------------

  useEffect(() => {
    const TIME = 500;
    if (visible) {
      setTimeout(() => {
        setShowChat(true);
      }, TIME);
    }
  });

  // -----------------------------------------------------------------------------------------------------------
  return (
    <div
      className="transition-5"
      style={{
        ...styles.chatEngineWindow,
        ...{ height: visible ? '100%' : '0px', zIndex: visible ? '100' : '0' },
      }}
    >
      {showChat && (
        <ChatEngineWrapper>
          <Socket
            projectID={process.env.REACT_APP_CE_PROJECT_ID}
            userName={user.email}
            userSecret={user.email}
          />
          <ChatFeed activeChat={chat.id} />
        </ChatEngineWrapper>
      )}
    </div>
  );
}
