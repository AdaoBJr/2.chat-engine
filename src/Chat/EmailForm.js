import React, { useState } from 'react';
import { styles } from './styles';
import axios from 'axios';

import { LoadingOutlined } from '@ant-design/icons';

export default function EmailForm({ visible, setUser, setChat }) {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);

  const getOrCreateUser = (callback) => {
    axios
      .put(
        'https://api.chatengine.io/users/',
        {
          username: email,
          secret: email,
          email: email,
        },
        {
          headers: {
            'Private-Key': process.env.REACT_APP_CE_PRIVATE_KEY,
          },
        }
      )
      .then((response) => callback(response.data))
      .catch((error) => console.log('User Error:', error));
  };

  const getOrCreateChat = (callback) => {
    axios
      .put(
        'https://api.chatengine.io/chats/',
        { usernames: [email, 'AdaoBJr'], is_direct_chat: true },
        {
          headers: {
            'Project-ID': process.env.REACT_APP_CE_PROJECT_ID,
            'User-Name': email,
            'User-Secret': email,
          },
        }
      )
      .then((response) => callback(response.data))
      .catch((error) => console.log('Chat Error:', error));
  };

  const handleSumit = (e) => {
    e.preventDefault();
    setLoading(true);

    console.log('Enviando Email para ChatEngine', email);

    getOrCreateUser((user) => {
      setUser && setUser(user);
      getOrCreateChat((chat) => {
        setLoading(false);
        setChat && setChat(chat);
      });
    });
  };

  return (
    <div
      style={{
        ...styles.emailFormWindow,
        ...{
          height: visible ? '100%' : '0px',
          opacity: visible ? '1' : '0',
        },
      }}
    >
      <div style={{ height: '0px' }}>
        <div style={styles.stripe} />
      </div>

      <div
        style={{
          ...styles.loadingDiv,
          ...{
            zIndex: loading ? '10' : '-1',
            opacity: loading ? '0.33' : '0',
          },
        }}
      >
        <LoadingOutlined
          className="transition-5"
          style={{
            ...styles.loadingIcon,
            ...{
              zIndex: loading ? '10' : '-1',
              opacity: loading ? '1' : '0',
              fontSize: '82px',
              top: 'calc(50% - 41px)',
              left: 'calc(50% - 41px)',
            },
          }}
        />
      </div>

      <div style={styles.emailContent}>
        <form onSubmit={(e) => handleSumit(e)} style={styles.emailForm}>
          <input
            type="text"
            placeholder="Entre com seu email"
            onChange={(e) => setEmail(e.target.value)}
            style={styles.emailInput}
          />
        </form>
        <div style={styles.bottomText}>
          Entre com seu email <br /> para iniciar
        </div>
      </div>
    </div>
  );
}
