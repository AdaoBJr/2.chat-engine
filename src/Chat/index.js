import React, { useState } from 'react';
import { styles } from './styles';

import ChatEngine from './ChatEngine';
import EmailForm from './EmailForm';

export default function Chat() {
  const [user, setUser] = useState(null);
  const [chat, setChat] = useState(null);

  const handleUser = (user) => {
    setUser(user);
  };

  const handleChat = (chat) => {
    setChat(chat);
  };

  return (
    <div style={styles.chatWindow}>
      <EmailForm
        visible={user === null || chat === null}
        setUser={handleUser}
        setChat={handleChat}
      />
      <ChatEngine visible={user !== null && chat !== null} user={user} chat={chat} />
    </div>
  );
}
